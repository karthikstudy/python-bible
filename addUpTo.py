def add_up_to(n):
    total = 0
    for i in range(0, n + 1, 1):
        total += i
    return total


print(add_up_to(4))
