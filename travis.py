known_users = ["karthik", "kiran", "ramu", "manoj"]

while True:
    print("Hi! My name is Travis")
    name = input("What is your name?: ").strip().lower()

    if name in known_users:
        print("Hello {}!".format(name))
        remove = input("Would you like to be removed from the system (y/n)?: ").strip().lower()
        
        if remove == "y":
            known_users.remove(name)
        elif remove == "n":
            print("FO!")
    else:
        print("Can't recognize you {}".format(name))
        add_me = input("Would you like to added to the system (y/n)?: ").strip().lower()
        if add_me == 'y':
            known_users.append(name)
        elif add_me == "n":
            print("FO!")